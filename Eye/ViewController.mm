//
//  ViewController.m
//  Eye
//
//  Created by Pawel Malijewski-POL on 03/07/2017.
//  Copyright © 2017 C2E Atlas. All rights reserved.
//

#import "ViewController.h"

#import <opencv2/opencv.hpp>
//#import "opencv2/highgui/ios.h"
//#import <opencv2/operations.hpp>
#import <opencv2/core.hpp>

using namespace cv;


@interface ViewController ()
{
    CvVideoCamera* videoCamera;
}

@property (nonatomic, retain) CvVideoCamera* videoCamera;

@end

@implementation ViewController

@synthesize videoCamera;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
        
    self.videoCamera = [[CvVideoCamera alloc] initWithParentView:_imageView];
    self.videoCamera.delegate = self;
    self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack; //AVCaptureDevicePositionFront;

    //aself.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset352x288;
    self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.videoCamera.defaultFPS = 30;
    self.videoCamera.grayscaleMode = NO;
    
    self.videoCamera.rotateVideo = YES;

    [self.videoCamera start];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

void drawRaw(const cv::Mat& flow, cv::Mat& cflowmap, int step, const cv::Scalar& color)
{
    for(int y = 0; y < cflowmap.rows; y += step)
        for(int x = 0; x < cflowmap.cols; x += step)
        {
            const cv::Point2f& fxy = flow.at<cv::Point2f>(y, x);
            arrowedLine(cflowmap, cv::Point(x,y), cv::Point(cvRound(x+fxy.x), cvRound(y+fxy.y)), color, 0.5);
        }
}

void drawFiltered(const cv::Mat& flow, cv::Mat& cflowmap, int step, const cv::Scalar& color)
{
    // calc avarage move:
    int ptCount = 0;
    double sum = 0;
    
    for(int y = 0; y < cflowmap.rows; y += step)
        for(int x = 0; x < cflowmap.cols; x += step)
        {
            const cv::Point2f& fxy = flow.at<cv::Point2f>(y, x);
            sum += ( fxy.y*fxy.y + fxy.x*fxy.x );
            ++ptCount;
        }
    
    double avg = sum / ptCount;
    
    // draw vectors
    for(int y = 0; y < cflowmap.rows; y += step)
        for(int x = 0; x < cflowmap.cols; x += step)
        {
            const cv::Point2f& fxy = flow.at<cv::Point2f>(y, x);
            
            if ( ( fxy.y*fxy.y + fxy.x*fxy.x ) < 10 * avg || ( fxy.y*fxy.y + fxy.x*fxy.x ) > 3000 )
            {
                // filter out small/large vectors
                continue;
            }
            
            arrowedLine(cflowmap, cv::Point(x,y), cv::Point(cvRound(x+fxy.x), cvRound(y+fxy.y)), color, 0.5);
            //pma line(cflowmap, cv::Point(x,y), cv::Point(cvRound(x+fxy.x), cvRound(y+fxy.y)), color);
            //pma circle(cflowmap, cv::Point(x,y), 1, CV_RGB(200, 0, 0), -1);
        }
}

void drawFiltered2(const cv::Mat& flow, cv::Mat& cflowmap, int step, const cv::Scalar& color)
{
    for(int y = 0; y < cflowmap.rows; y += step)
        for(int x = 0; x < cflowmap.cols; x += step)
        {
            const cv::Point2f& fxy = flow.at<cv::Point2f>(y, x);
            
            cv::Point p1 = cv::Point(x,y);
            cv::Point p2 = cv::Point(cvRound(x+fxy.x), cvRound(y+fxy.y));
            
            double hypotenuse = sqrt( (double)(p1.y - p2.y)*(p1.y - p2.y) + (double)(p1.x - p2.x)*(p1.x - p2.x) );
            
            float ratio = std::abs(p1.x - p2.x)/std::abs(p1.y - p2.y);
            
            if (hypotenuse > 15 && ratio > 0.5 && ratio != 0)
                line(cflowmap, cv::Point(x,y), cv::Point(cvRound(x+fxy.x), cvRound(y+fxy.y)),
                     color);
            //circle(cflowmap, cv::Point(x,y), 1, CV_RGB(200, 0, 0), -1);
        }
    
}

- (void)processImage:(Mat&)image;
{
    static cv::Mat prev_image;
    cv::Mat flow;
    std::vector<cv::Mat> channels;
    split(image, channels);
    cv::Mat downsampled_grey( image.rows, image.cols, CV_8UC1 );;

    cv::Mat output[] = { downsampled_grey };
    int from_to[] = { 0,0 };
    cv::mixChannels(&image, 1, output, 1, from_to, 1);
    
    cv::cvtColor(image, downsampled_grey, CV_BGR2GRAY);
    
    if (prev_image.empty()) {
        downsampled_grey.copyTo(prev_image);
    }

    //    calcOpticalFlowFarneback( InputArray prev, InputArray next,
    //                                                             flow, pyr_scale, levels, winsize, iterations, poly_n, poly_sigma, flags );
    //  cv::calcOpticalFlowFarneback(prev_image, downsampled_grey, flow, 0.2,       3,      14,      3,          5,      1.2,        0);
    cv::calcOpticalFlowFarneback(prev_image, downsampled_grey,     flow, 0.2,       3,      14,      3,          5,      1.2,        0 );
    
    switch ( self.segmentedCtrl.selectedSegmentIndex )
    {
        case 0:
            drawRaw(flow, image, 12, CV_RGB(0, 255, 0) );
            break;
            
        case 1:
            drawFiltered(flow, image, 12, CV_RGB(0, 255, 0) );
            break;
            
        case 2:
            drawFiltered2(flow, image, 12, CV_RGB(0, 255, 0) );
            break;
            
        default:
            NSLog( @"Unsupported switch value");
            break;
    }
    
    downsampled_grey.copyTo(prev_image);
}


- (IBAction)valueChanged:(id)sender {
}
@end
