//
//  AppDelegate.h
//  Eye
//
//  Created by Pawel Malijewski-POL on 03/07/2017.
//  Copyright © 2017 C2E Atlas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

