//
//  ViewController.h
//  Eye
//
//  Created by Pawel Malijewski-POL on 03/07/2017.
//  Copyright © 2017 C2E Atlas. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import <opencv2/videoio/cap_ios.h>

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface ViewController : UIViewController <CvVideoCameraDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedCtrl;

- (IBAction)valueChanged:(id)sender;


@end

