//
//  main.m
//  Eye
//
//  Created by Pawel Malijewski-POL on 03/07/2017.
//  Copyright © 2017 C2E Atlas. All rights reserved.
//

#ifdef __cplusplus
#import <opencv2/opencv.hpp>
#endif

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
